import confetti from 'canvas-confetti'

import { data } from './data'
import useEffectAfter from '../hooks/useEffectAfter'
import ProgressUnit from './progress-unit'

const Progress = ({ completed }: { completed: string[] }) => {
  const metrics = {
    Units: {
      current: completed.reduce((prev, curr) => prev + data[curr].credits, 0),
      goal: 60,
    },
    Required: {
      current: completed.filter((course) => !data[course].elective).length,
      goal: Object.values(data).filter((course) => !course.elective).length,
    },
    Electives: {
      current: completed.filter((course) => data[course].elective).length,
      goal: 3,
    },
  }

  useEffectAfter(
    2,
    () => {
      const allComplete = () => {
        const currents = Object.values(metrics).reduce(
          (prev, { current }) => prev + current,
          0
        )
        const goals = Object.values(metrics).reduce(
          (prev, { goal }) => prev + goal,
          0
        )
        return currents >= goals
      }
      if (allComplete()) {
        const duration = 5 * 1000
        const animationEnd = Date.now() + duration
        const defaults = {
          startVelocity: 30,
          spread: 360,
          ticks: 60,
          zIndex: 0,
        }

        const randrange = (min: number, max: number) => {
          return Math.random() * (max - min) + min
        }

        const interval: NodeJS.Timer = setInterval(function () {
          const timeLeft = animationEnd - Date.now()

          if (timeLeft <= 0) {
            return clearInterval(interval)
          }

          const particleCount = 50 * (timeLeft / duration)
          // since particles fall down, start a bit higher than random
          confetti(
            Object.assign({}, defaults, {
              particleCount,
              origin: { x: randrange(0.1, 0.3), y: Math.random() - 0.2 },
            })
          )
          confetti(
            Object.assign({}, defaults, {
              particleCount,
              origin: { x: randrange(0.7, 0.9), y: Math.random() - 0.2 },
            })
          )
        }, 250)
      }
    },
    [completed]
  )

  return (
    <header className="bg-inherit mx-auto flex items-center justify-center h-5/6">
      {Object.entries(metrics).map(([k, v]) => (
        <ProgressUnit
          key={k}
          current={v['current']}
          goal={v['goal']}
          units={k}
        />
      ))}
    </header>
  )
}

export default Progress
