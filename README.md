# Prerequisites Visualizer for OSU CS Classes
https://reqs.zdsfa.com/

## Features
- Hover/tap to show a class' prerequisites and what it is a prerequisite for
- Mark classes as complete by clicking/double-tapping
- Confetti when all grad requirements are met
- Dark/light mode switch
